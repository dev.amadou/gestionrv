package com.example.gestionrv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class GestionRvApplication {

	@GetMapping("/")
	public String home(){
		return "HELLO WORD PICSOU!";
	}

    public static void main(String[] args) {
        SpringApplication.run(GestionRvApplication.class, args);
    }

}
