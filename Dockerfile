FROM openjdk:8-jdk-alpine
VOLUME /tmp
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]

#FROM nginx:latest
#RUN sed -i 's/nginx/xavki/g' /usr/share/nginx/html/index.html
#EXPOSE 80